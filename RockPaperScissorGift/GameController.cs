﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorGift
{
    class GameController
    {
        private readonly List<Player> players;

        private readonly int totalNumberOfRound;
        private int currentRound;

        private List<MatchReferee> currentRoundMatches;

        public GameController(List<Player> players)
        {
            Console.OutputEncoding = Encoding.UTF8;

            this.players = players;

            totalNumberOfRound = players.Count % 2 == 0 ? players.Count - 1 : players.Count;
        }

        public void Start()
        {
            Console.Clear();

            GameView.DisplayRules();
            GameView.DisplayParticipantsChoices(players);

            Console.ReadLine();
            Console.Clear();

            void DisplayMatches()
            {
                foreach (var match in currentRoundMatches)
                {
                    GameView.DisplayLineSeparator();
                    match.DisplayMatchOutcome();
                }
            }

            for (var i = 0; i < totalNumberOfRound; i++)
            {
                currentRoundMatches = new List<MatchReferee>();

                foreach (var playerPair in GetPlayerPairs())
                {
                    var match = new MatchReferee(playerPair.Item1, playerPair.Item2);
                    match.ResolveMatch(DisplayMatches);
                    currentRoundMatches.Insert(0, match);
                }

                // Console.Clear();

                GameView.DisplayCurrentRound(++currentRound, totalNumberOfRound);
                GameView.DisplayParticipantsScore(players);


                Console.ReadLine();
                //Console.Clear();
            }

            GameView.DisplayCurrentRound(currentRound, totalNumberOfRound);
            GameView.DisplayParticipantsScore(players);
        }

        private IEnumerable<(Player, Player)> GetPlayerPairs()
        {
            var tempPlayers = new List<Player>(players);

            if (tempPlayers.Count % 2 != 0)
                tempPlayers.Add(new Player());
            var numberOfGamesPerRound = tempPlayers.Count / 2;
            tempPlayers.RemoveAt(0);
            

            if (!tempPlayers[currentRound % tempPlayers.Count].IsDummy)
            {
                var playerOne = players[0];
                var playerTwo = tempPlayers[currentRound % tempPlayers.Count];

                yield return (playerOne, playerTwo);
            }
            for (var i = 1; i < numberOfGamesPerRound; i++)
            {
                var playerOne = tempPlayers[(currentRound + i) % tempPlayers.Count];
                var playerTwo = tempPlayers[(currentRound + tempPlayers.Count - i) % tempPlayers.Count];
                if (playerOne.IsDummy || playerTwo.IsDummy)
                    continue;

                yield return (playerOne, playerTwo);
            }
        }
    }
}
