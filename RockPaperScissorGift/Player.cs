using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RockPaperScissorGift
{
    public class Player
    {
        public string Name { get; }
        public bool IsDummy { get; }
        public Choice[] Choices { get; }
        public int CurrentScore { get; set; }

        private List<Player> facedOpponents;

        public Player()
        {
            IsDummy = true;
            Name = "Mr.FancyTie";
            Choices = new Choice[] { Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy, Choice.Dummy };

            facedOpponents = new List<Player>();
        }

        public Player(string filePath)
        {
            Name = Path.GetFileNameWithoutExtension(filePath);
            Choices = FileReader.LoadChoices(filePath);

            facedOpponents = new List<Player>();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"{Name.ToUpper()}:");
            foreach (var choice in Choices)
            {
                sb.Append($" {choice.ToString()},");
            }

            sb.Remove(sb.Length-1, 1);
            return sb.ToString();
        }

        public void AddFacedOpponent(Player opponent)
        {
            if (facedOpponents.Contains(opponent))
                throw new InvalidDataException($"Player already faced of this opponent: {opponent}");

            facedOpponents.Add(opponent);
        }
    }
}