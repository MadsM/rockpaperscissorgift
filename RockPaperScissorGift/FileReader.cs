using System.IO;
using System.Linq;

namespace RockPaperScissorGift
{
    public static class FileReader
    {
        public static Choice[] LoadChoices(string filePath)
        {
            var lines = File.ReadAllLines(filePath);

            return lines.Select(StringToChoice).ToArray();
        }

        private static Choice StringToChoice(string input)
        {
            switch (input.Trim().ToUpper())
            {
                case "R":
                    return Choice.Rock;
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissor;
                default:
                    return Choice.Paper;
            }
        }
    }
}