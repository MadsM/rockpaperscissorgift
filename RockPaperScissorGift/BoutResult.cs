﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorGift
{
    public class BoutResult
    {
        public enum MatchOutcomeType
        {
            PlayerOneWon,
            PlayerTwoWon,
            Tie
        }

        private string playerNameOne;
        private string playerNameTwo;

        public Choice PlayerOneChoice { get; set; }
        public Choice PlayerTwoChoice { get; set; }

        public MatchOutcomeType MatchOutcome { get; set; }

        public BoutResult(Player playerOne, Player playerTwo, int boutNumber)
        {
            this.playerNameOne = playerOne.Name;
            this.playerNameTwo = playerTwo.Name;
            this.PlayerOneChoice = playerOne.Choices[boutNumber];
            this.PlayerTwoChoice = playerTwo.Choices[boutNumber];
        }
    }
}
