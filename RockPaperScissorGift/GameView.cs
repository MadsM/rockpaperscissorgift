﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RockPaperScissorGift.BoutResult;

namespace RockPaperScissorGift
{
    public class GameView
    {
        //private const string Rock = "{} ";
        //private const string Paper = "—— ";
        //private const string Scissor = "8< ";
        //private const string Dummy = "?? ";

        private const string Rock = "✊";
        private const string Paper = "✋";
        private const string Scissor = "✌";
        private const string Dummy = "🖕";


        private Player playerOne;
        private Player playerTwo;

        private int LongestPlayerNameLength => Math.Max(playerOne.Name.Length, playerTwo.Name.Length);

        public GameView(Player playerOne, Player playerTwo)
        {
            this.playerOne = playerOne;
            this.playerTwo = playerTwo;

        }

        public void DisplayMatchProgress(IEnumerable<BoutResult> boutResults)
        {
            ClearLine(0);
            DisplayPlayerName(playerOne, LongestPlayerNameLength);
            DisplayPlayerChoice(true, boutResults);

            LineBreak();

            ClearLine(1);
            DisplayPlayerName(playerTwo, LongestPlayerNameLength);
            DisplayPlayerChoice(false, boutResults);

            LineBreak();
        }

        public void DisplayMatchResults(IEnumerable<BoutResult> boutResults, MatchOutcomeType matchOutcome)
        {
            DisplayPlayerName(playerOne, LongestPlayerNameLength, GetOutcomeColor(true, matchOutcome));
            DisplayPlayerChoice(true, boutResults);
            DisplayPlayerScore(boutResults.Count(bout => bout.MatchOutcome == MatchOutcomeType.PlayerOneWon));

            LineBreak();

            DisplayPlayerName(playerTwo, LongestPlayerNameLength, GetOutcomeColor(false, matchOutcome));
            DisplayPlayerChoice(false, boutResults);
            DisplayPlayerScore(boutResults.Count(bout => bout.MatchOutcome == MatchOutcomeType.PlayerTwoWon));

            LineBreak();
        }

        public static void DisplayParticipantsChoices(IEnumerable<Player> players)
        {
            var longestName = players.Max(p => p.Name.Length);
            foreach (var player in players)
            {
                DisplayPlayerName(player, longestName);
                
                foreach (var choice in player.Choices)
                {
                    DisplayChoice(choice, ConsoleColor.White);
                }

                LineBreak();
            }
        }

        public static void DisplayParticipantsScore(IEnumerable<Player> players)
        {
            var longestName = players.Max(p => p.Name.Length);
            foreach (var player in players.OrderByDescending(p => p.CurrentScore))
            {
                DisplayPlayerName(player, longestName);
                DisplayPlayerScore(player.CurrentScore);

                LineBreak();
            }
        }

        public static void DisplayLineSeparator()
        {
            Console.WriteLine($"===================================================================");
        }

        public static void DisplayCurrentRound(int round, int maxRound)
        {
            Console.WriteLine($"==================== ROUND: {round} OF {maxRound} ==============================");
        }

        private static void DisplayPlayerScore(int score)
        {
            Console.Write($": {score}");
        }

        private static void DisplayPlayerName(Player player, int nameLength, ConsoleColor playerColor = ConsoleColor.White)
        {
            Console.ForegroundColor = playerColor;

            ClearLine(Console.CursorTop);

            var paddedName = player.Name.PadRight(nameLength);

            Console.Write($"{paddedName} ");
        }

        private void DisplayPlayerChoice(bool isPlayerOne, IEnumerable<BoutResult> boutResult)
        {
            foreach (var bout in boutResult)
            {
                DisplayChoice(isPlayerOne ? bout.PlayerOneChoice : bout.PlayerTwoChoice, GetOutcomeColor(isPlayerOne, bout.MatchOutcome));
            }
        }

        private ConsoleColor GetOutcomeColor(bool isPlayerOne, MatchOutcomeType matchOutcome)
        {
            switch (matchOutcome)
            {
                case MatchOutcomeType.PlayerOneWon:
                    return isPlayerOne ? ConsoleColor.Green : ConsoleColor.Red;
                case MatchOutcomeType.PlayerTwoWon:
                    return isPlayerOne ? ConsoleColor.Red : ConsoleColor.Green;
                case MatchOutcomeType.Tie:
                    return ConsoleColor.Yellow;
                default:
                    throw new NotImplementedException();
            }
        }

        private static void ClearLine(int line)
        {
            Console.SetCursorPosition(0, line);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, line);
        }

        private static void DisplayChoice(Choice choice, ConsoleColor consoleColor)
        {
            Console.ForegroundColor = consoleColor;
            Console.Write(ConvertChoiceToString(choice));
            RestoreConsoleForegroundColor();
        }


        private static string ConvertChoiceToString(Choice choice)
        {
            switch (choice)
            {
                case Choice.Rock:
                    return Rock;
                case Choice.Paper:
                    return Paper;
                case Choice.Scissor:
                    return Scissor;
                case Choice.Dummy:
                    return Dummy;
                default:
                    throw new NotImplementedException();
            }
        }

        private static void LineBreak()
        {
            Console.Write("\n");
        }

        private static void RestoreConsoleForegroundColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void DisplayRules()
        {
            Console.WriteLine($"Welcome to Rock, Paper, Scissor, Gift!");
            Console.WriteLine($"{Rock} = Rock");
            Console.WriteLine($"{Paper} = Paper");
            Console.WriteLine($"{Scissor} = Scissor");
            Console.WriteLine($"(obviously!)");
            DisplayLineSeparator();
        }
    }
}
