﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static RockPaperScissorGift.BoutResult;

namespace RockPaperScissorGift
{
    public class MatchReferee
    {
        private readonly GameView view;

        private readonly Player playerOne;
        private readonly Player playerTwo;
        private readonly int matchLength;

        private List<BoutResult> matchResults;

        public MatchReferee(Player playerOne, Player playerTwo)
        {
            this.playerOne = playerOne;
            this.playerTwo = playerTwo;
            this.view = new GameView(playerOne, playerTwo);

            matchLength = playerOne.Choices.Count();
            matchResults = new List<BoutResult>();
        }

        public void ResolveMatch(Action displayAction)
        {
            for (int i = 0; i < matchLength; i++)
            {
                matchResults.Add(ResolveBout(i));
                view.DisplayMatchProgress(matchResults);
                displayAction?.Invoke();
                Thread.Sleep(50);
            }

            Console.Clear();

            var matchOutcome = GetMatchOutcome();
            RecordMatchOutcome(matchOutcome);
            view.DisplayMatchResults(matchResults, matchOutcome);
            displayAction?.Invoke();
        }

        public void DisplayMatchOutcome()
        {
            view.DisplayMatchResults(matchResults, GetMatchOutcome());
        }

        private BoutResult ResolveBout(int boutNumber)
        {
            var boutResult = new BoutResult(playerOne, playerTwo, boutNumber);

            if (playerOne.IsDummy || playerTwo.IsDummy)
            {
                boutResult.MatchOutcome = MatchOutcomeType.Tie;
                return boutResult;
            }

            switch (playerOne.Choices[boutNumber])
            {
                case Choice.Paper when playerTwo.Choices[boutNumber] == Choice.Paper:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.Tie; break;
                case Choice.Paper when playerTwo.Choices[boutNumber] == Choice.Rock:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerOneWon; break;
                case Choice.Paper when playerTwo.Choices[boutNumber] == Choice.Scissor:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerTwoWon; break;

                case Choice.Rock when playerTwo.Choices[boutNumber] == Choice.Paper:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerTwoWon; break;
                case Choice.Rock when playerTwo.Choices[boutNumber] == Choice.Rock:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.Tie; break;
                case Choice.Rock when playerTwo.Choices[boutNumber] == Choice.Scissor:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerOneWon; break;

                case Choice.Scissor when playerTwo.Choices[boutNumber] == Choice.Paper:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerOneWon; break;
                case Choice.Scissor when playerTwo.Choices[boutNumber] == Choice.Rock:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.PlayerTwoWon; break;
                case Choice.Scissor when playerTwo.Choices[boutNumber] == Choice.Scissor:
                    boutResult.MatchOutcome = BoutResult.MatchOutcomeType.Tie; break;
            }

            return boutResult;
        }

        private MatchOutcomeType GetMatchOutcome()
        {
            if (playerOne.IsDummy || playerTwo.IsDummy)
                return MatchOutcomeType.Tie;

            var playerOneWins = matchResults.Count(bout => bout.MatchOutcome == MatchOutcomeType.PlayerOneWon);
            var playerTwoWins = matchResults.Count(bout => bout.MatchOutcome == MatchOutcomeType.PlayerTwoWon);

            if (playerOneWins == playerTwoWins)
                return MatchOutcomeType.Tie;

            return playerOneWins > playerTwoWins ? MatchOutcomeType.PlayerOneWon : MatchOutcomeType.PlayerTwoWon;
        }

        private void RecordMatchOutcome(MatchOutcomeType matchOutcome)
        {
            playerOne.AddFacedOpponent(playerTwo);
            playerTwo.AddFacedOpponent(playerOne);

            if (matchOutcome == MatchOutcomeType.PlayerOneWon)
                playerOne.CurrentScore += 3;

            if (matchOutcome == MatchOutcomeType.PlayerTwoWon)
                playerTwo.CurrentScore += 3;

            if (matchOutcome == MatchOutcomeType.Tie)
            {
                playerOne.CurrentScore += 1;
                playerTwo.CurrentScore += 1;
            }
        }
    }
}
