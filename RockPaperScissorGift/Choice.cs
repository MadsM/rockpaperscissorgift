namespace RockPaperScissorGift
{
    public enum Choice
    {
        Rock,
        Paper,
        Scissor,
        Dummy
    }
}