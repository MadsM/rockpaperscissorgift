﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RockPaperScissorGift
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var paths = Directory.EnumerateFiles("./ChoiceFiles");
            var players = new List<Player>();
            foreach (var path in paths)
            {
                if (!Path.HasExtension(path) || Path.GetExtension(path) != ".txt")
                    continue;
                
                Console.WriteLine($"Loading player from: {path}");
                var player = new Player(path);
                players.Add(player);
                Console.WriteLine(player);
            }

            var gameController = new GameController(players);
            gameController.Start();

 
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
        }
    }
}